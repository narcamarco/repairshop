import React, { useEffect } from 'react';

const useTitle = (title) => {
  useEffect(() => {
    console.log('mount');
    const prevTitle = document.title;
    document.title = title;

    return () => {
      console.log('unmount');
      document.title = prevTitle;
    };
  }, [title]);
};

export default useTitle;
