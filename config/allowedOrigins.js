const allowedOrigins = [
  'http://localhost:3000',
  'https://repairshop-7qys.onrender.com',
];

module.exports = allowedOrigins;
